# ChickTechAIWorkshopNCSU

This is the tutorial for the ChickTech AI Workshop at NCSU on 5/18/19

### GitLab

1.  Sign into your [GitLab](https://gitlab.com) account or create one if you don't already have one.
2.  Fork the repo by going to [https://gitlab.com/staylornc/chicktechaiworkshopncsu](https://gitlab.com/staylornc/chicktechaiworkshopncsu)
3.  In your new repo...
3.  Copy the Clone URL
4.  ![Gitlab](assets/images/Clone.png)
3.  Open the GitGUI App.
4.  Hit "Clone Existing Repository" and paste the URL for the source location.
5.  For Target Directory, browse to your documents folder and then add a "/chicktechaiworkshopncsu"
5.  ![Gitlab](assets/images/GITGUI.png)

Note: If you are doing this at home, I would recommend using SourceTree in place of GitGUI

### Install Anaconda
Note: This has already been on the lab machines. If you decide to play with this on a different machine that cannot support the full anaconda installation, instead install miniconda



# Useful Links
[GitLab](https://gitlab.com) - Code repository. Continue to add projects here to create your portfolio  
[Anaconda](https://www.anaconda.com/distribution/) - Data Science Development Environment  
[Markdown Cheatsheet](https://www.markdownguide.org/basic-syntax/) - The language used for Readme files in your GitLab projects and Jupyter notebook text blocks
[SourceTree](https://www.sourcetreeapp.com/) - This is a source control client that will help you manange your projects

# Next Steps
1.  Complete the [Jupyter Notebook tutorial](https://www.dataquest.io/blog/jupyter-notebook-tutorial/)
2.  Explore some of the tutorials on [Kaggle](https://www.kaggle.com/)
2.  Take a look at this [tutorial](https://www.datacamp.com/community/tutorials/recommender-systems-python)
2.  Perform the same exercise with a different dataset 